package com.simply.mowy;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.model.ReminderBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static com.simply.mowy.model.ReminderPriority.CRITICAL;
import static com.simply.mowy.model.ReminderPriority.HIGH;
import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DatabaseIntegrationTests {

    @Before
    public void cleanDB() {
        Reminder.deleteAll(Reminder.class);
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.simply.mowy", appContext.getPackageName());
    }

    @Test
    public void testReminderDatabaseIntegration() {
        int nrRemindersToSave = 20;
        for (int i = 0; i < nrRemindersToSave; i++) {
            new ReminderBuilder()
                    .setText("Reminder " + i)
                    .setPriority(HIGH.getValue())
                    .build().save();
        }

        assertEquals(Reminder.listAll(Reminder.class).size(), nrRemindersToSave);

        List<Reminder> reminders = Reminder.findWithQuery(Reminder.class, "Select * from Reminder where text = ?", "Reminder 1");

        assertEquals(reminders.size(), 1);
        Reminder reminder = reminders.get(0);
        Assert.assertEquals(reminder.getPriority(), HIGH.getValue());

        String newText = "New Reminder Title";
        reminder.setText(newText);
        reminder.setPriority(CRITICAL.getValue());
        reminder.save();

        reminders = Reminder.findWithQuery(Reminder.class, "Select * from Reminder where text = ?", "Reminder 1");
        assertEquals(reminders.size(), 0);

        reminders = Reminder.findWithQuery(Reminder.class, "Select * from Reminder where text = ?", newText);
        assertEquals(reminders.size(), 1);
        reminder = reminders.get(0);
        Assert.assertEquals(reminder.getPriority(), CRITICAL.getValue());
    }

}
