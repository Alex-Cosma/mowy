package com.simply.mowy.jobs;

import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.Direction;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;

import com.simply.mowy.R;
import com.simply.mowy.SimplyMowyApplication;
import com.simply.mowy.utilities.SharedPreferencesService;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Alex on 2/28/2017.
 */
public class ApplicationUsageServiceTest {
    private static final long TIMEOUT = 5000;

    private ApplicationUsageService applicationUsageService;
    private SharedPreferencesService sharedPreferencesService;

    @Before
    public void setUp() throws Exception {
        applicationUsageService = ((SimplyMowyApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getComponentFactory().getApplicationUsageService();
        sharedPreferencesService = ((SimplyMowyApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getComponentFactory().getSharedPreferencesService();
    }

    @Test
    public void runApplicationUsageNotificationCheck() throws Exception {
        DateTime twoDaysAgo = new DateTime().minusDays(2);
        sharedPreferencesService.setLastLoginTime(twoDaysAgo);
        assertTrue(applicationUsageService.shouldSendNotificationAboutApplicationUsage());

        applicationUsageService.runApplicationUsageNotificationCheck();

        String notificationTitle = InstrumentationRegistry.getTargetContext().getApplicationContext().getResources().getString(R.string.application_usage_notification_title); // TODO: find better way to store these things
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        device.openNotification();
        device.wait(Until.hasObject(By.text(notificationTitle)), TIMEOUT);
        UiObject2 title = device.findObject(By.text(notificationTitle));
        assertEquals(notificationTitle, title.getText());
        title.swipe(Direction.RIGHT, 1f);

        DateTime twentyThreeHoursAgo = new DateTime().minusHours(23);
        sharedPreferencesService.setLastLoginTime(twentyThreeHoursAgo);
        assertFalse(applicationUsageService.shouldSendNotificationAboutApplicationUsage());

        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        device.openNotification();
        device.wait(Until.hasObject(By.text(notificationTitle)), TIMEOUT);
        title = device.findObject(By.text(notificationTitle));
        assertNull(title);
    }

    @Test
    public void shouldSendNotificationAboutApplicationUsage() throws Exception {
        DateTime twoDaysAgo = new DateTime().minusDays(2);
        sharedPreferencesService.setLastLoginTime(twoDaysAgo);
        assertTrue(applicationUsageService.shouldSendNotificationAboutApplicationUsage());

        DateTime twentyThreeHoursAgo = new DateTime().minusHours(23);
        sharedPreferencesService.setLastLoginTime(twentyThreeHoursAgo);
        assertFalse(applicationUsageService.shouldSendNotificationAboutApplicationUsage());

        DateTime twentyFiveHoursAgo = new DateTime().minusHours(25);
        sharedPreferencesService.setLastLoginTime(twentyFiveHoursAgo);
        assertTrue(applicationUsageService.shouldSendNotificationAboutApplicationUsage());

        DateTime today = new DateTime();
        sharedPreferencesService.setLastLoginTime(today);
        assertFalse(applicationUsageService.shouldSendNotificationAboutApplicationUsage());
    }

    @Test
    public void sendNotificationAboutApplicationUsage() throws Exception {
        DateTime twoDaysAgo = new DateTime().minusDays(2);
        sharedPreferencesService.setLastLoginTime(twoDaysAgo);

        applicationUsageService.runApplicationUsageNotificationCheck();

        String notificationTitle = InstrumentationRegistry.getTargetContext().getApplicationContext().getResources().getString(R.string.application_usage_notification_title);
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        device.openNotification();
        device.wait(Until.hasObject(By.text(notificationTitle)), TIMEOUT);
        UiObject2 title = device.findObject(By.text(notificationTitle));
        assertEquals(notificationTitle, title.getText());
        title.swipe(Direction.RIGHT, 1f);
    }

}