package com.simply.mowy.services;

import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.Direction;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;

import com.simply.mowy.R;
import com.simply.mowy.SimplyMowyApplication;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.model.ReminderBuilder;
import com.simply.mowy.model.ReminderPriority;
import com.simply.mowy.notifications.NotificationService;

import org.junit.Before;
import org.junit.Test;

import static com.simply.mowy.model.ReminderPriority.CRITICAL;
import static com.simply.mowy.model.ReminderPriority.HIGH;
import static com.simply.mowy.model.ReminderPriority.LOW;
import static com.simply.mowy.model.ReminderPriority.MEDIUM;
import static com.simply.mowy.model.ReminderPriority.VERY_LOW;
import static com.simply.mowy.model.ReminderPriority.getPriorityByValue;
import static com.simply.mowy.model.ReminderPriority.values;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Alex on 2/20/2017.
 */
public class NotificationServiceTest {
    private static final long TIMEOUT = 5000;
    private static final long TIMEOUT_LESS = 1000;

    private NotificationService notificationService;

    @Before
    public void setUp() throws Exception {
        notificationService = ((SimplyMowyApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getComponentFactory().getNotificationService();
    }

    @Test
    public void notifyForReminder() throws Exception {
        for (ReminderPriority priority : values()) {
            Reminder reminder = new ReminderBuilder()
                    .setText("Test Title " + priority.getName())
                    .setPriority(priority.getValue())
                    .build();
            reminder.save();

            notificationService.notifyForReminder(reminder);

            String notificationTitle = getPriorityByValue(reminder.getPriority()).getName() + " notification!";
            String notificationText = reminder.getText();
            UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            device.openNotification();
            device.wait(Until.hasObject(By.text(notificationTitle)), TIMEOUT);
            UiObject2 title = device.findObject(By.text(notificationTitle));
            UiObject2 text = device.findObject(By.text(notificationText));
            assertEquals(notificationTitle, title.getText());
            assertEquals(notificationText, text.getText());
            title.swipe(Direction.RIGHT, 1f);
        }
    }

    @Test
    public void markReminderAsDone() throws Exception {
        for (ReminderPriority priority : values()) {
            Reminder reminder = new ReminderBuilder()
                    .setText("Test Title " + priority.getName())
                    .setPriority(priority.getValue())
                    .build();
            reminder.save();

            notificationService.notifyForReminder(reminder);

            String notificationTitle = getPriorityByValue(reminder.getPriority()).getName() + " notification!";
            String notificationText = reminder.getText();
            UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            device.openNotification();
            device.wait(Until.hasObject(By.text(notificationTitle)), TIMEOUT);
            UiObject2 title = device.findObject(By.text(notificationTitle));
            UiObject2 text = device.findObject(By.text(notificationText));
            assertEquals(notificationTitle, title.getText());
            assertEquals(notificationText, text.getText());

            Reminder notificationReminder = Reminder.findById(Reminder.class, reminder.getId());
            assertNotNull(notificationReminder);
            UiObject2 doneButton = device.findObject(By.text(InstrumentationRegistry.getTargetContext().getString(R.string.done)));
            doneButton.click();
            device.wait(Until.gone(By.text(notificationText)), TIMEOUT_LESS);

            notificationReminder = Reminder.findById(Reminder.class, reminder.getId());
            assertNull(notificationReminder);
        }
    }

    @Test
    public void increasePriorityTest() throws Exception {
        ReminderPriority[] priorityValues = new ReminderPriority[]{VERY_LOW, LOW, MEDIUM, HIGH};
        for (ReminderPriority priority : priorityValues) {
            Integer initialPriorityValue = priority.getValue();
            Reminder reminder = new ReminderBuilder().setText("Test Title " + priority.getName())
                    .setPriority(initialPriorityValue)
                    .build();
            reminder.save();

            notificationService.notifyForReminder(reminder);

            String notificationTitle = getPriorityByValue(reminder.getPriority()).getName() + " notification!";
            String notificationText = reminder.getText();
            UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            device.openNotification();
            device.wait(Until.hasObject(By.text(notificationTitle)), TIMEOUT);
            UiObject2 title = device.findObject(By.text(notificationTitle));
            UiObject2 text = device.findObject(By.text(notificationText));
            assertEquals(notificationTitle, title.getText());
            assertEquals(notificationText, text.getText());

            Reminder notificationReminder = Reminder.findById(Reminder.class, reminder.getId());
            assertNotNull(notificationReminder);
            assertEquals(notificationReminder.getPriority(), initialPriorityValue);
            UiObject2 increasePriorityButton = device.findObject(By.text(InstrumentationRegistry.getTargetContext().getString(R.string.increase_priority)));
            increasePriorityButton.click();
            device.wait(Until.gone(By.text(notificationText)), TIMEOUT_LESS);

            notificationReminder = Reminder.findById(Reminder.class, notificationReminder.getId());
            assertEquals(notificationReminder.getPriority(), Integer.valueOf(initialPriorityValue + 1));
        }
    }

    @Test
    public void decreasePriorityTest() throws Exception {
        ReminderPriority[] priorityValues = new ReminderPriority[]{LOW, MEDIUM, HIGH, CRITICAL};
        for (ReminderPriority priority : priorityValues) {
            Integer initialPriorityValue = priority.getValue();
            Reminder reminder = new ReminderBuilder().setText("Test Title " + priority.getName())
                    .setPriority(initialPriorityValue)
                    .build();
            reminder.save();

            notificationService.notifyForReminder(reminder);

            String notificationTitle = getPriorityByValue(reminder.getPriority()).getName() + " notification!";
            String notificationText = reminder.getText();
            UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            device.openNotification();
            device.wait(Until.hasObject(By.text(notificationTitle)), TIMEOUT);
            UiObject2 title = device.findObject(By.text(notificationTitle));
            UiObject2 text = device.findObject(By.text(notificationText));
            assertEquals(notificationTitle, title.getText());
            assertEquals(notificationText, text.getText());

            Reminder notificationReminder = Reminder.findById(Reminder.class, reminder.getId());
            assertNotNull(notificationReminder);
            assertEquals(notificationReminder.getPriority(), initialPriorityValue);
            UiObject2 decreasePriorityButton = device.findObject(By.text(InstrumentationRegistry.getTargetContext().getString(R.string.decrease_priority)));
            decreasePriorityButton.click();
            device.wait(Until.gone(By.text(notificationText)), TIMEOUT_LESS);

            notificationReminder = Reminder.findById(Reminder.class, notificationReminder.getId());
            assertEquals(notificationReminder.getPriority(), Integer.valueOf(initialPriorityValue - 1));
        }
    }


}