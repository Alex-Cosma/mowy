package com.simply.mowy.services;

import android.support.test.InstrumentationRegistry;

import com.simply.mowy.SimplyMowyApplication;
import com.simply.mowy.jobs.ReminderActivationService;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.model.ReminderBuilder;
import com.simply.mowy.model.ReminderPriority;
import com.simply.mowy.utilities.DateTimeService;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static com.simply.mowy.utilities.Constants.MIN_ACTIVATION_PER_PRIORITY;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Alex on 2/19/2017.
 */
public class ReminderActivationServiceTest {

    private ReminderActivationService reminderActivationService;
    private DateTimeService dateTimeService;

    @Before
    public void setUp() throws Exception {
        Reminder.deleteAll(Reminder.class);
        reminderActivationService = ((SimplyMowyApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getComponentFactory().getReminderActivationService();
        dateTimeService = ((SimplyMowyApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getComponentFactory().getDateTimeService();
    }

    @Test
    public void runActivationForReminders() throws Exception {
        //TODO: later test it, for now we test things with the method below
    }

    @Test
    public void shouldActivateReminder() throws Exception {
        for (ReminderPriority priority : ReminderPriority.values()) {
            DateTime priorityTime = new DateTime().minusMinutes((int) (MIN_ACTIVATION_PER_PRIORITY.get(priority.getValue()) - 1));
            Reminder reminder = new ReminderBuilder()
                    .setText(priority.toString())
                    .setPriority(priority.getValue())
                    .setLastActivationTime(priorityTime)
                    .build();
            reminder.save();

            assertFalse(reminderActivationService.shouldActivateReminder(reminder));

            reminder.setLastActivationTime(priorityTime.minusMinutes(1));
            assertTrue(reminderActivationService.shouldActivateReminder(reminder));

            Reminder syncdReminder = Reminder.findById(Reminder.class, reminder.getId());
            assertFalse(reminderActivationService.shouldActivateReminder(syncdReminder));
        }

    }

    @Test
    public void shouldActivateReminderWithStartDate() throws Exception {
        for (ReminderPriority priority : ReminderPriority.values()) {
            DateTime priorityTime = new DateTime().minusMinutes((int) (MIN_ACTIVATION_PER_PRIORITY.get(priority.getValue()) - 1));
            Reminder reminder = new ReminderBuilder()
                    .setText(priority.toString())
                    .setPriority(priority.getValue())
                    .setLastActivationTime(priorityTime)
                    .setStartDate(dateTimeService.now().plusDays(1))
                    .build();
            reminder.save();

            assertFalse(reminderActivationService.shouldActivateReminder(reminder));

            reminder.setLastActivationTime(priorityTime.minusMinutes(1));
            assertFalse(reminderActivationService.shouldActivateReminder(reminder));

            reminder.setStartDate(dateTimeService.now().minusMinutes(1));
            assertTrue(reminderActivationService.shouldActivateReminder(reminder));

            Reminder syncdReminder = Reminder.findById(Reminder.class, reminder.getId());
            assertFalse(reminderActivationService.shouldActivateReminder(syncdReminder));
        }
    }

}