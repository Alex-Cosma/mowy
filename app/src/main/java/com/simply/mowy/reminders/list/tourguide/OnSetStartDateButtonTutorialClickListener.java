package com.simply.mowy.reminders.list.tourguide;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;
import com.simply.mowy.reminders.list.adapters.listeners.OnSetStartDateButtonClickListener;
import com.simply.mowy.utilities.DateTimeService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Alex on 3/14/2017.
 */

class OnSetStartDateButtonTutorialClickListener extends OnSetStartDateButtonClickListener {

    OnSetStartDateButtonTutorialClickListener(RemindersArrayAdapter adapter, RemindersActivity activity, List<Reminder> reminders, ReminderService reminderService, DateTimeService dateTimeService) {
        super(adapter, activity, reminders, reminderService, dateTimeService);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        ((RemindersArrayTourGuideAdapter) adapter).switchToDeleteStep();
        DateTime startDate = dateTimeService.formDate(year, monthOfYear, dayOfMonth);
        Reminder reminder = reminders.get(position);
        reminderService.setStartDateToReminder(reminder, startDate);
        reminder.setPinned(false);
        adapter.notifyItemChanged(position);
    }

}
