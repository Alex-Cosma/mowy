package com.simply.mowy.reminders.list.adapters.listeners;

import android.view.View;

import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;
import com.simply.mowy.utilities.Constants;
import com.simply.mowy.utilities.DateTimeService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Alex on 4/11/2017.
 */

public class OnSetDueDateButtonClickListener extends AutoEntrySwiper implements View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private final RemindersArrayAdapter adapter;
    private final List<Reminder> reminders;
    private final ReminderService reminderService;
    private final DateTimeService dateTimeService;
    private final RemindersActivity activity;
    private int position;
    private DateTime dueDate;

    public OnSetDueDateButtonClickListener(RemindersArrayAdapter adapter, List<Reminder> reminders, ReminderService reminderService, DateTimeService dateTimeService, RemindersActivity activity) {
        this.adapter = adapter;
        this.reminders = reminders;
        this.reminderService = reminderService;
        this.dateTimeService = dateTimeService;
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        View actualView = RecyclerViewAdapterUtils.getParentViewHolderItemView(view);
        this.position = activity.getRecyclerView().getChildAdapterPosition(actualView);
        Calendar defaultSelectedDate = getDefaultSelectedDate();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                defaultSelectedDate.get(Calendar.YEAR),
                defaultSelectedDate.get(Calendar.MONTH),
                defaultSelectedDate.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(Calendar.getInstance());
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(activity.getFragmentManager(), Constants.DUE_DATE_DATE_PICKER_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        this.dueDate = dateTimeService.formDate(year, monthOfYear, dayOfMonth);

        DateTime now = dateTimeService.now();

        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.getHourOfDay(),
                now.getMinuteOfDay(),
                now.getSecondOfDay(),
                true
        );
        tpd.setMinTime(now.getHourOfDay(), now.getMinuteOfDay(), now.getSecondOfDay());
        tpd.setVersion(TimePickerDialog.Version.VERSION_2);
        tpd.show(activity.getFragmentManager(), Constants.DUE_DATE_TIME_PICKER_DIALOG_TAG);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        dueDate = dueDate.withHourOfDay(hourOfDay);
        dueDate = dueDate.withMinuteOfHour(minute);
        dueDate = dueDate.withSecondOfMinute(second);
        reminderService.setDueDateToReminder(reminders.get(position), dueDate);
        adapter.notifyItemChanged(position);
        swipeAfterDelay(adapter, position);
    }

    private Calendar getDefaultSelectedDate() {
        Reminder reminder = reminders.get(position);
        Calendar defaultSelectedDate = Calendar.getInstance();
        if (reminder.hasStartDate()) {
            defaultSelectedDate.set(Calendar.YEAR, reminder.getStartDate().getYear());
            defaultSelectedDate.set(Calendar.MONTH, reminder.getStartDate().getMonthOfYear() - 1);
            defaultSelectedDate.set(Calendar.DAY_OF_MONTH, reminder.getStartDate().getDayOfMonth());
        }
        return defaultSelectedDate;
    }

}
