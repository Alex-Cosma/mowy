package com.simply.mowy.reminders.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.kobakei.ratethisapp.RateThisApp;
import com.simply.mowy.R;
import com.simply.mowy.SimplyMowyApplication;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.notifications.NotificationService;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;
import com.simply.mowy.utilities.DateTimeService;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Module;

@Module
public class RemindersActivity extends AppCompatActivity {

    @Inject
    protected NotificationService notificationService;

    @Inject
    protected ReminderService reminderService;

    @Inject
    protected DateTimeService dateTimeService;

    @BindView(R.id.btnAddReminder)
    protected FloatingActionButton addReminderBtn;
    @BindView(R.id.rvReminders)
    protected RecyclerView remindersRecyclerView;

    protected RemindersArrayAdapter remindersAdapter;
    protected List<Reminder> reminders;

    @BindView(R.id.pbProgress)
    protected ProgressBar progressBar;

    private ReminderActivityBroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders);

        ButterKnife.bind(this);
        ((SimplyMowyApplication) getApplicationContext()).getComponentFactory().inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeRecyclerView();
        addReminderBtn.setOnClickListener(getActionListenerForAddReminderButton());

        receiver = new ReminderActivityBroadcastReceiver(this, progressBar);
    }

    @Override
    protected void onResume() {
        registerReceiver(receiver, receiver.fetchIntentFilterForReminderActions());
        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeRatingSystem();
    }

    public void refreshRemindersList() {
        reminders.clear();
        reminders.addAll(reminderService.getAllReminders());
        remindersAdapter.notifyDataSetChanged();
    }

    private void initializeRecyclerView() {
        reminders = reminderService.getAllReminders();
        remindersAdapter = getRemindersAdapter();
        RecyclerViewSwipeManager swipeMgr = new RecyclerViewSwipeManager();
        remindersRecyclerView.setHasFixedSize(true);
        remindersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        remindersRecyclerView.setAdapter(swipeMgr.createWrappedAdapter(remindersAdapter));
        remindersRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        swipeMgr.attachRecyclerView(remindersRecyclerView);
    }

    @NonNull
    protected View.OnClickListener getActionListenerForAddReminderButton() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addReminder("");
                addReminderBtn.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward));
            }
        };
    }

    protected RemindersArrayAdapter getRemindersAdapter() {
        return new RemindersArrayAdapter(this, this.reminders, reminderService, dateTimeService);
    }

    protected void addReminder(String title) {
        Reminder reminder = reminderService.createNewReminder(title);
        reminderService.persistReminder(reminder);
        reminders.add(0, reminder);
        remindersAdapter.notifyDataSetChanged();
        remindersRecyclerView.smoothScrollToPosition(0);
    }

    public RecyclerView getRecyclerView() {
        return remindersRecyclerView;
    }

    private void initializeRatingSystem() {
        RateThisApp.onStart(this);
        RateThisApp.showRateDialogIfNeeded(this);
    }
}
