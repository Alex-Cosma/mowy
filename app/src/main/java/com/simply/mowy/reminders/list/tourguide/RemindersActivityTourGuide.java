package com.simply.mowy.reminders.list.tourguide;

import android.support.design.widget.FloatingActionButton;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.simply.mowy.R;
import com.simply.mowy.model.ReminderPriority;

import tourguide.tourguide.Overlay;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by Alex on 2/23/2017.
 */

class RemindersActivityTourGuide extends TourGuide {

    private final RemindersTourGuideActivity activity;
    private SparseArray<String> priorityDescriptionMap = new SparseArray<>();

    RemindersActivityTourGuide(RemindersTourGuideActivity activity) {
        super(activity);
        this.activity = activity;
        setupTourGuide();
        priorityDescriptionMap.put(0, activity.getString(R.string.tutorial_spinner_very_low));
        priorityDescriptionMap.put(1, activity.getString(R.string.tutorial_spinner_low));
        priorityDescriptionMap.put(2, activity.getString(R.string.tutorial_spinner_medium));
        priorityDescriptionMap.put(3, activity.getString(R.string.tutorial_spinner_high));
        priorityDescriptionMap.put(4, activity.getString(R.string.tutorial_spinner_critical));
    }

    void startTourGuideTutorial(final RemindersTourGuideActivity activity) {
        final View btnContinueTutorial = activity.findViewById(R.id.btnContinueTutorial);
        final View btnSkipTutorial = activity.findViewById(R.id.btnSkipTutorial);
        btnContinueTutorial.setVisibility(VISIBLE);
        btnSkipTutorial.setVisibility(VISIBLE);

        btnContinueTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUp();
                btnContinueTutorial.setVisibility(GONE);
                btnSkipTutorial.setVisibility(GONE);
                playTourGuideForFirstEntry(activity);
            }
        });

        btnSkipTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.endFirstTimeRun();
            }
        });
        setOverlay(new Overlay().disableClick(false));
        playOn(btnContinueTutorial);
    }

    private void playTourGuideForFirstEntry(final RemindersTourGuideActivity activity) {
        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation())
                .setGravity(Gravity.TOP | Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL)
                .setTitle(activity.getString(R.string.tutorial_start_title)).setDescription(activity.getString(R.string.tutorial_start_description)));
        setOverlay(new Overlay().setStyle(Overlay.Style.NoHole).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemindersActivityTourGuide.this.cleanUp();
                activity.startTour();
            }
        }));
        playOn(activity.findViewById(R.id.tutorial_center_view));
    }

    void playTourGuideForFloatingActionButton(FloatingActionButton fab) {
        cleanUp();
        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation()).setGravity(Gravity.TOP | Gravity.START).setDescription(activity.getString(R.string.tutorial_fab_description)));
        setOverlay(new Overlay().setStyle(Overlay.Style.Circle));
        playOn(fab);
    }

    void playTourGuideForListItem(final View listItem, final MaterialSpinner spinner, final EditText editText) {
        spinner.setClickable(false);
        spinner.setEnabled(false);
        editText.setEnabled(false);
        editText.setClickable(false);

        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation()).setGravity(Gravity.BOTTOM | Gravity.CENTER).setTitle(activity.getString(R.string.tutorial_reminder_title)).setDescription(activity.getString(R.string.tutorial_reminder_description)));
        setOverlay(new Overlay().setStyle(Overlay.Style.Rectangle)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RemindersActivityTourGuide.this.cleanUp();
                        editText.setClickable(true);
                        editText.setEnabled(true);
                        playTourGuideForPrioritiesSpinner(spinner);
                    }
                })
        );
        playOn(listItem);
    }

    private void playTourGuideForPrioritiesSpinner(final MaterialSpinner spinner) {
        int spinnerChildren = spinner.getItems().size();
        playTourGuideForSpinnerValues(spinner, 0, spinnerChildren);
    }

    private void playTourGuideForSpinnerValues(final MaterialSpinner spinner, final int currentChild, final int maxCount) {
        spinner.setSelectedIndex(currentChild);
        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation()).setGravity(Gravity.CENTER | Gravity.START)
                .setTitle(ReminderPriority.getPriorityByValue(currentChild).getName() + " priority")
                .setDescription(priorityDescriptionMap.get(currentChild)));
        setOverlay(new Overlay().setStyle(Overlay.Style.Rectangle)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RemindersActivityTourGuide.this.cleanUp();
                        if (currentChild < maxCount - 1) {
                            int nextChild = currentChild + 1;
                            playTourGuideForSpinnerValues(spinner, nextChild, maxCount);
                        } else {
                            RemindersActivityTourGuide.this.cleanUp();
                            spinner.setEnabled(true);
                            spinner.setClickable(true);
                            playTourGuideForNotification();
                        }
                    }
                })
        );
        playOn(spinner);
    }

    private void playTourGuideForNotification() {
        activity.notifyForExistingReminder();
        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation())
                .setGravity(Gravity.TOP | Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL)
                .setTitle(activity.getString(R.string.tutorial_notification_middle_title)).setDescription(activity.getString(R.string.tutorial_notification_middle_description)));
        setOverlay(new Overlay().setStyle(Overlay.Style.NoHole));
        playOn(activity.findViewById(R.id.tutorial_center_view));
    }

    void playTourGuideForSettingStartDate(View listItem) {
        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation()).setGravity(Gravity.BOTTOM | Gravity.CENTER).setTitle(activity.getString(R.string.tutorial_set_start_date_title)).setDescription(activity.getString(R.string.tutorial_set_start_date_description)));
        setOverlay(new Overlay().setStyle(Overlay.Style.Rectangle));
        playOn(listItem);
    }

    void playTourGuideForDeletingEntry(View listItem) {
        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation()).setGravity(Gravity.BOTTOM | Gravity.CENTER).setTitle(activity.getString(R.string.tutorial_delete_title)).setDescription(activity.getString(R.string.tutorial_delete_description)));
        setOverlay(new Overlay().setStyle(Overlay.Style.Rectangle));
        playOn(listItem);
    }

    void playTourGuideEnding(final RemindersTourGuideActivity activity) {
        cleanUp();
        setToolTip(new ToolTip().setEnterAnimation(getBaseAnimation())
                .setGravity(Gravity.TOP | Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL)
                .setTitle(activity.getString(R.string.tutorial_ending_title)).setDescription(activity.getString(R.string.tutorial_ending_description)));
        setOverlay(new Overlay().setStyle(Overlay.Style.NoHole).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RemindersActivityTourGuide.this.cleanUp();
                activity.endFirstTimeRun();
            }
        }));
        playOn(activity.findViewById(R.id.tutorial_center_view));
    }


    private Animation getBaseAnimation() {
        Animation baseAnimation = new AlphaAnimation(0.50f, 1f);
        baseAnimation.setDuration(400);
        baseAnimation.setFillAfter(true);
        return baseAnimation;
    }

    private void setupTourGuide() {
        with(TourGuide.Technique.Click);
        setOverlay(new Overlay().disableClick(true));
    }
}
