package com.simply.mowy.reminders.list.adapters;

/**
 * Created by Alex on 3/6/2017.
 */
public interface ReminderChangedListener {

    void reminderChanged(int position);

}
