package com.simply.mowy.reminders.list.adapters;

import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.simply.mowy.R;
import com.simply.mowy.reminders.list.adapters.listeners.ReminderPriorityChangedListener;
import com.simply.mowy.reminders.list.adapters.listeners.ReminderTextChangedListener;
import com.simply.mowy.reminders.list.adapters.listeners.SoftKeyboardCloser;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alex on 3/10/2017.
 */

public class ReminderViewHolder extends AbstractSwipeableItemViewHolder {
    @BindView(R.id.etReminderText)
    public EditText text;
    @BindView(R.id.spnnrReminderPriority)
    public MaterialSpinner priority;
    @BindView(R.id.btnSetStartDate)
    public Button btnSetStartDate;
    @BindView(R.id.tvStartDateLabel)
    public TextView tvStartDateLabel;

    @BindView(R.id.btnSetDueDate)
    public Button btnSetDueDate;
    @BindView(R.id.tvDueDateLabel)
    public TextView tvDueDateLabel;
    @BindView(R.id.tvDueDateTime)
    public TextView tvDueDateTime;

    @BindView(R.id.container)
    RelativeLayout containerView;
    @BindView(R.id.rlBehindViews)
    RelativeLayout rlBehindViews;

    ReminderPriorityChangedListener reminderPriorityChangedListener;
    ReminderTextChangedListener reminderTextChangedListener;
    SoftKeyboardCloser softKeyboardCloser;

    ReminderViewHolder(View view, ReminderPriorityChangedListener reminderPriorityChangedListener, ReminderTextChangedListener reminderTextChangedListener, SoftKeyboardCloser softKeyboardCloser) {
        super(view);
        this.softKeyboardCloser = softKeyboardCloser;
        ButterKnife.bind(this, view);
        this.reminderPriorityChangedListener = reminderPriorityChangedListener;
        this.reminderTextChangedListener = reminderTextChangedListener;
        text.addTextChangedListener(reminderTextChangedListener);
        priority.setOnItemSelectedListener(reminderPriorityChangedListener);
    }

    @Override
    public View getSwipeableContainerView() {
        return containerView;
    }

    @Override
    public void onSlideAmountUpdated(float horizontalAmount, float verticalAmount, boolean isSwiping) {
        float alpha = 1.0f - Math.min(Math.max(Math.abs(horizontalAmount), 0.0f), 1.0f);
        ViewCompat.setAlpha(containerView, alpha);
        if (horizontalAmount > 0) {
            ViewCompat.setAlpha(rlBehindViews, (2f - 2 * alpha));
        } else {
            ViewCompat.setAlpha(rlBehindViews, 0);
        }
    }
}
