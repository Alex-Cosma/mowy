package com.simply.mowy.reminders.list.adapters.listeners;

import android.text.Editable;
import android.text.TextWatcher;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;

import java.util.List;

/**
 * Created by Alex on 2/18/2017.
 */

public class ReminderTextChangedListener implements TextWatcher {

    private final ReminderService reminderService;
    private final List<Reminder> reminders;
    private final SoftKeyboardCloser softKeyboardCloser;
    private Reminder reminder;

    public ReminderTextChangedListener(ReminderService reminderService, List<Reminder> reminders, SoftKeyboardCloser softKeyboardCloser) {
        this.reminderService = reminderService;
        this.reminders = reminders;
        this.softKeyboardCloser = softKeyboardCloser;
    }

    public void updatePosition(int position) {
        this.reminder = reminders.get(position);
    }

    @Override
    public void afterTextChanged(Editable text) {
        if (!text.toString().equals(reminder.getText())) {
            reminderService.setTextToReminder(reminder, text.toString());
            softKeyboardCloser.scheduleKeyboardClose();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start,
                                  int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start,
                              int before, int count) {
        softKeyboardCloser.cancelScheduledKeyboardClose();
    }

}
