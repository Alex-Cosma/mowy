package com.simply.mowy.reminders.list;

import android.content.Context;
import android.content.Intent;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.model.ReminderBuilder;
import com.simply.mowy.model.ReminderPriority;
import com.simply.mowy.utilities.AnalyticsService;
import com.simply.mowy.utilities.DateTimeService;

import org.joda.time.DateTime;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.simply.mowy.model.ReminderPriority.AUTO;
import static com.simply.mowy.utilities.Constants.Actions.SHOW_LOADING_FOR_ACTIVITY;
import static com.simply.mowy.utilities.Constants.Analytics.Actions.ADD_REMINDER;
import static com.simply.mowy.utilities.Constants.Analytics.Actions.DECREASE_PRIORITY;
import static com.simply.mowy.utilities.Constants.Analytics.Actions.INCREASE_PRIORITY;
import static com.simply.mowy.utilities.Constants.Analytics.Actions.REMOVE_REMINDER;
import static com.simply.mowy.utilities.Constants.Analytics.Actions.SET_PRIORITY;
import static com.simply.mowy.utilities.Constants.Analytics.Categories.REMINDERS;
import static java.lang.Math.abs;

/**
 * Created by Alex on 2/21/2017.
 */
@Singleton
public class ReminderService {

    private final DateTimeService dateTimeService;
    private final AnalyticsService analyticsService;
    private final Context context;

    @Inject
    ReminderService(DateTimeService dateTimeService, AnalyticsService analyticsService, Context context) {
        this.dateTimeService = dateTimeService;
        this.analyticsService = analyticsService;
        this.context = context;
    }

    List<Reminder> getAllReminders() {
        return sortRemindersByPriority(Reminder.listAll(Reminder.class));
    }

    Reminder createNewReminder(String title) {
        analyticsService.sendEvent(REMINDERS, ADD_REMINDER);
        return new ReminderBuilder()
                .setText(title)
                .setPriority(abs(new Random().nextInt(5)))
                .setLastActivationTime(dateTimeService.now())
                .build();
    }

    void persistReminder(Reminder reminder) {
        reminder.save();
        notifyForProgress();
    }

    public void deleteReminderById(long reminderId) {
        Reminder reminder = Reminder.findById(Reminder.class, reminderId);
        if (reminder != null) {
            reminder.delete();
            analyticsService.sendEvent(REMINDERS, REMOVE_REMINDER);
            notifyForProgress();
        }
    }

    public void increasePriority(long reminderId) {
        Reminder reminder = Reminder.findById(Reminder.class, reminderId);
        Integer reminderPriority = reminder.getPriority();
        if (reminderPriority < ReminderPriority.CRITICAL.getValue()) {
            reminder.setPriority(++reminderPriority);
            persistReminder(reminder);
            analyticsService.sendEvent(REMINDERS, INCREASE_PRIORITY, reminder.getText());
        }
    }

    public void decreasePriority(long reminderId) {
        Reminder reminder = Reminder.findById(Reminder.class, reminderId);
        Integer reminderPriority = reminder.getPriority();
        if (reminderPriority > ReminderPriority.VERY_LOW.getValue()) {
            reminder.setPriority(--reminderPriority);
            persistReminder(reminder);
            analyticsService.sendEvent(REMINDERS, DECREASE_PRIORITY, reminder.getText());
        }
    }

    public void resetLastActivationTime(Reminder reminder) {
        reminder.setLastActivationTime(dateTimeService.now());
        persistReminder(reminder);
    }

    public void setTextToReminder(Reminder reminder, String text) {
        reminder.setText(text);
        persistReminder(reminder);
    }

    public void setPriorityToReminder(Reminder reminder, int priority) {
        reminder.setPriority(priority);
        analyticsService.sendEvent(REMINDERS, SET_PRIORITY, reminder.getText());
        persistReminder(reminder);
    }

    public void setStartDateToReminder(Reminder reminder, DateTime startDate) {
        reminder.setStartDate(startDate);
        persistReminder(reminder);
    }

    public void setDueDateToReminder(Reminder reminder, DateTime dueDate) {
        changePriorityIfFirstAssignedDueDate(reminder);
        reminder.setDueDate(dueDate);
        persistReminder(reminder);
    }

    private void changePriorityIfFirstAssignedDueDate(Reminder reminder) {
        if (!reminder.hasDueDate()) {
            reminder.setPriority(AUTO.getValue());
        }
    }

    private void notifyForProgress() {
        Intent intent = new Intent();
        intent.setAction(SHOW_LOADING_FOR_ACTIVITY);
        context.sendBroadcast(intent);
    }

    private List<Reminder> sortRemindersByPriority(List<Reminder> reminders) {
        Collections.sort(reminders, new Comparator<Reminder>() {
            @Override
            public int compare(Reminder o1, Reminder o2) {
                return o2.getPriority().compareTo(o1.getPriority());
            }
        });
        return reminders;
    }

}
