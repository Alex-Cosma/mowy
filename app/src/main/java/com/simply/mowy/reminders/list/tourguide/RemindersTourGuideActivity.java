package com.simply.mowy.reminders.list.tourguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.simply.mowy.R;
import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;

import static com.simply.mowy.utilities.Constants.BundleExtras.FROM_TUTORIAL_NOTIFICATION;

/**
 * Created by Alex on 2/23/2017.
 */

public class RemindersTourGuideActivity extends RemindersActivity {

    private RemindersActivityTourGuide tourGuide;
    private boolean afterNotificationClickedState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        tourGuide = new RemindersActivityTourGuide(this);
        Bundle extras = getIntent().getExtras();
        afterNotificationClickedState = extras != null && extras.getBoolean(FROM_TUTORIAL_NOTIFICATION);
        super.onCreate(savedInstanceState);
        if (!afterNotificationClickedState) {
            tourGuide.startTourGuideTutorial(this);
        }
    }

    public void startTour() {
        tourGuide.playTourGuideForFloatingActionButton(addReminderBtn);
    }

    public void endFirstTimeRun() {
        Intent myIntent = new Intent(RemindersTourGuideActivity.this, RemindersActivity.class);
        RemindersTourGuideActivity.this.startActivity(myIntent);
        finish();
    }

    public void notifyForExistingReminder() {
        notificationService.notifyForTutorialReminder(reminders.get(0));
    }

    @NonNull
    protected View.OnClickListener getActionListenerForAddReminderButton() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addReminder(getResources().getString(R.string.tutorial_notification_description_text));
                addReminderBtn.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward));
                tourGuide.cleanUp();
            }
        };
    }

    protected RemindersArrayAdapter getRemindersAdapter() {
        return new RemindersArrayTourGuideAdapter(this, this.reminders, reminderService, dateTimeService, tourGuide, afterNotificationClickedState);
    }
}
