package com.simply.mowy.reminders.list.adapters.listeners;

import android.os.Handler;

import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;
import com.simply.mowy.reminders.list.adapters.listeners.swipe.SwipeResultActionUnpin;

import static com.simply.mowy.utilities.Constants.DELAY_FOR_UNPINNING_AFTER_DATE_SELECTION;

/**
 * Created by Alex on 4/12/2017.
 */

class AutoEntrySwiper {
    void swipeAfterDelay(final RemindersArrayAdapter adapter, final int position) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new SwipeResultActionUnpin(adapter, position).performAction();
            }
        }, DELAY_FOR_UNPINNING_AFTER_DATE_SELECTION);
    }
}
