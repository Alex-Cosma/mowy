package com.simply.mowy.reminders.list.adapters.listeners.swipe;

import android.app.Activity;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem;
import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.simply.mowy.utilities.Constants.PENDING_REMOVAL_TIMEOUT_FOR_DELETING_REMINDER;

/**
 * Created by Alex on 3/10/2017.
 */

public class SwipeResultActionRemove extends SwipeResultActionRemoveItem {
    protected final Activity activity;
    private final List<Reminder> reminders;
    private final ReminderService reminderService;
    private final RemindersArrayAdapter adapter;
    private final Handler handler;
    private final HashMap<Reminder, Runnable> pendingRunnables;
    private final List<Reminder> remindersPendingRemoval;
    private int position;

    public SwipeResultActionRemove(RemindersArrayAdapter adapter, List<Reminder> reminders, Activity activity, ReminderService reminderService) {
        this.adapter = adapter;
        this.reminders = reminders;
        this.activity = activity;
        this.reminderService = reminderService;
        remindersPendingRemoval = new ArrayList<>();
        pendingRunnables = new HashMap<>();
        handler = new Handler();
    }


    @Override
    protected void onPerformAction() {
        pendingRemoval(position);
    }

    public void pendingRemoval(final int position) {
        final Reminder reminder = reminders.get(position);
        showUndoSnackbar(position, reminder);
        if (!remindersPendingRemoval.contains(reminder)) {
            remindersPendingRemoval.add(reminder);
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(remindersPendingRemoval.indexOf(reminder));
                }
            };
            reminders.remove(position);
            adapter.notifyItemRemoved(position);
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT_FOR_DELETING_REMINDER);
            pendingRunnables.put(reminder, pendingRemovalRunnable);
        }
    }

    private void showUndoSnackbar(final int position, final Reminder reminder) {
        Snackbar.make(activity.findViewById(R.id.tutorial_center_view),
                R.string.reminder_deleted, Snackbar.LENGTH_INDEFINITE).setDuration(PENDING_REMOVAL_TIMEOUT_FOR_DELETING_REMINDER).setAction(R.string.undo, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                restoreReminder(reminder, position);
            }
        }).show();
    }

    private void restoreReminder(Reminder reminder, int position) {
        Runnable pendingRemovalRunnable = pendingRunnables.get(reminder);
        pendingRunnables.remove(reminder);
        if (pendingRemovalRunnable != null) {
            handler.removeCallbacks(pendingRemovalRunnable);
        }
        remindersPendingRemoval.remove(reminder);
        reminders.add(position, reminder);
        adapter.notifyItemInserted(position);
        showRestoreSnackbar();
    }

    private void remove(int position) {
        final Reminder reminder = remindersPendingRemoval.get(position);
        if (remindersPendingRemoval.contains(reminder)) {
            remindersPendingRemoval.remove(reminder);
        }
        reminderService.deleteReminderById(reminder.getId());
    }

    private void showRestoreSnackbar() {
        Snackbar.make(activity.findViewById(R.id.tutorial_center_view), R.string.reminder_restored, Snackbar.LENGTH_SHORT).show();
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
