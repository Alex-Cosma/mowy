package com.simply.mowy.reminders.list.tourguide;

import android.view.ViewGroup;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.adapters.ReminderViewHolder;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;
import com.simply.mowy.utilities.DateTimeService;

import java.util.List;

/**
 * Created by Alex on 2/18/2017.
 */

class RemindersArrayTourGuideAdapter extends RemindersArrayAdapter {

    private final RemindersActivityTourGuide tourGuide;
    private final boolean afterNotificationClickedState;
    private boolean setDateTutorialDone = false;

    RemindersArrayTourGuideAdapter(RemindersTourGuideActivity activity, List<Reminder> reminders, ReminderService reminderService, DateTimeService dateTimeService, RemindersActivityTourGuide tourGuide, boolean afterNotificationClickedState) {
        super(activity, reminders, reminderService, dateTimeService);
        this.tourGuide = tourGuide;
        this.afterNotificationClickedState = afterNotificationClickedState;
        this.swipeResultActionRemove = new SwipeResultActionRemoveForTourGuide(this, reminders, activity, reminderService, tourGuide);
        this.onSetStartDateButtonClickListener = new OnSetStartDateButtonTutorialClickListener(this, activity, reminders, reminderService, dateTimeService);
    }

    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReminderViewHolder reminderViewHolder = super.onCreateViewHolder(parent, viewType);
        if (!afterNotificationClickedState) {
            tourGuide.playTourGuideForListItem(reminderViewHolder.itemView, reminderViewHolder.priority, reminderViewHolder.text);
        } else {
            if (!setDateTutorialDone) {
                tourGuide.playTourGuideForSettingStartDate(reminderViewHolder.itemView);
            } else {
                tourGuide.playTourGuideForDeletingEntry(reminderViewHolder.itemView);
            }
        }
        return reminderViewHolder;
    }

    @Override
    public int onGetSwipeReactionType(ReminderViewHolder holder, int position, int x, int y) {
        if (afterNotificationClickedState) {
            if (!setDateTutorialDone) {
                return SwipeableItemConstants.REACTION_CAN_SWIPE_RIGHT;
            } else {
                return SwipeableItemConstants.REACTION_CAN_SWIPE_LEFT;
            }
        } else {
            return SwipeableItemConstants.REACTION_CAN_NOT_SWIPE_ANY;
        }
    }

    void switchToDeleteStep() {
        this.setDateTutorialDone = true;
        tourGuide.cleanUp();
    }
}

