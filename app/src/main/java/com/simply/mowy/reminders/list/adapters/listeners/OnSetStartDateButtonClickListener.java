package com.simply.mowy.reminders.list.adapters.listeners;

import android.view.View;

import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;
import com.simply.mowy.utilities.Constants;
import com.simply.mowy.utilities.DateTimeService;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Alex on 3/14/2017.
 */

public class OnSetStartDateButtonClickListener extends AutoEntrySwiper implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    protected final RemindersArrayAdapter adapter;
    protected final List<Reminder> reminders;
    protected final ReminderService reminderService;
    protected final DateTimeService dateTimeService;
    private final RemindersActivity activity;
    protected int position;

    public OnSetStartDateButtonClickListener(RemindersArrayAdapter adapter, RemindersActivity activity, List<Reminder> reminders, ReminderService reminderService, DateTimeService dateTimeService) {
        this.adapter = adapter;
        this.activity = activity;
        this.reminders = reminders;
        this.reminderService = reminderService;
        this.dateTimeService = dateTimeService;
    }

    @Override
    public void onClick(View view) {
        View actualView = RecyclerViewAdapterUtils.getParentViewHolderItemView(view);
        this.position = activity.getRecyclerView().getChildAdapterPosition(actualView);
        Calendar defaultSelectedDate = getDefaultSelectedDate();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                defaultSelectedDate.get(Calendar.YEAR),
                defaultSelectedDate.get(Calendar.MONTH),
                defaultSelectedDate.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(Calendar.getInstance());
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.show(activity.getFragmentManager(), Constants.START_DATE_PICKER_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        DateTime startDate = dateTimeService.formDate(year, monthOfYear, dayOfMonth);
        Reminder reminder = reminders.get(position);
        reminderService.setStartDateToReminder(reminder, startDate);
        adapter.notifyItemChanged(position);
        swipeAfterDelay(adapter, position);
    }

    private Calendar getDefaultSelectedDate() {
        Reminder reminder = reminders.get(position);
        Calendar defaultSelectedDate = Calendar.getInstance();
        if (reminder.hasStartDate()) {
            defaultSelectedDate.set(Calendar.YEAR, reminder.getStartDate().getYear());
            defaultSelectedDate.set(Calendar.MONTH, reminder.getStartDate().getMonthOfYear() - 1);
            defaultSelectedDate.set(Calendar.DAY_OF_MONTH, reminder.getStartDate().getDayOfMonth());
        }
        return defaultSelectedDate;
    }

}
