package com.simply.mowy.reminders.list.tourguide;

import android.app.Activity;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;
import com.simply.mowy.reminders.list.adapters.listeners.swipe.SwipeResultActionRemove;

import java.util.List;

/**
 * Created by Alex on 3/14/2017.
 */

class SwipeResultActionRemoveForTourGuide extends SwipeResultActionRemove {
    private final RemindersActivityTourGuide tourGuide;

    SwipeResultActionRemoveForTourGuide(RemindersArrayAdapter adapter, List<Reminder> reminders, Activity activity, ReminderService reminderService, RemindersActivityTourGuide tourGuide) {
        super(adapter, reminders, activity, reminderService);
        this.tourGuide = tourGuide;
    }

    @Override
    public void pendingRemoval(final int position) {
        super.pendingRemoval(position);
        tourGuide.playTourGuideEnding((RemindersTourGuideActivity) activity);
    }
}
