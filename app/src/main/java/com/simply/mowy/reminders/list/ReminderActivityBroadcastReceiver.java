package com.simply.mowy.reminders.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.widget.ProgressBar;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.simply.mowy.utilities.Constants.Actions.DECREASE_PRIORITY_ACTION;
import static com.simply.mowy.utilities.Constants.Actions.FINISH_REMINDER_ACTION;
import static com.simply.mowy.utilities.Constants.Actions.INCREASE_PRIORITY_ACTION;
import static com.simply.mowy.utilities.Constants.Actions.SHOW_LOADING_FOR_ACTIVITY;
import static com.simply.mowy.utilities.Constants.PROGRESS_INDICATOR_VISIBLE_TIME;

/**
 * Created by Alex on 3/10/2017.
 */

public class ReminderActivityBroadcastReceiver extends BroadcastReceiver {

    private final RemindersActivity remindersActivity;
    private final ProgressBar progressBar;

    public ReminderActivityBroadcastReceiver(RemindersActivity remindersActivity, ProgressBar progressBar) {
        this.remindersActivity = remindersActivity;
        this.progressBar = progressBar;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        switch (action) {
            case FINISH_REMINDER_ACTION:
            case INCREASE_PRIORITY_ACTION:
            case DECREASE_PRIORITY_ACTION:
                showProgress(PROGRESS_INDICATOR_VISIBLE_TIME);
                refreshActivityRemindersList();
                break;
            case SHOW_LOADING_FOR_ACTIVITY:
                showProgress(PROGRESS_INDICATOR_VISIBLE_TIME);
                break;
        }

    }

    private void refreshActivityRemindersList() {
        Handler handler = new Handler();
        Runnable pendingRemovalRunnable = new Runnable() {
            @Override
            public void run() {
                remindersActivity.refreshRemindersList();
            }
        };
        handler.postDelayed(pendingRemovalRunnable, 350);
    }

    private void showProgress(int millis) {
        Handler handler = new Handler(Looper.getMainLooper());
        Runnable pendingRemovalRunnable = new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(INVISIBLE);
            }
        };
        handler.postDelayed(pendingRemovalRunnable, millis);
        remindersActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(VISIBLE);
            }
        });
    }

    public IntentFilter fetchIntentFilterForReminderActions() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(FINISH_REMINDER_ACTION);
        intentFilter.addAction(INCREASE_PRIORITY_ACTION);
        intentFilter.addAction(DECREASE_PRIORITY_ACTION);
        intentFilter.addAction(SHOW_LOADING_FOR_ACTIVITY);
        return intentFilter;
    }
}
