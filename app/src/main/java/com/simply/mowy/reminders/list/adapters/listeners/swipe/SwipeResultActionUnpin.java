package com.simply.mowy.reminders.list.adapters.listeners.swipe;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDefault;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;

/**
 * Created by Alex on 3/13/2017.
 */

public class SwipeResultActionUnpin extends SwipeResultActionDefault {
    private final int position;
    private RemindersArrayAdapter adapter;

    public SwipeResultActionUnpin(RemindersArrayAdapter adapter, int position) {
        this.adapter = adapter;
        this.position = position;
    }

    @Override
    protected void onPerformAction() {
        super.onPerformAction();

        Reminder item = adapter.getReminder(position);
        if (item.isPinned()) {
            item.setPinned(false);
            adapter.notifyItemChanged(position);
        }
    }

    @Override
    protected void onCleanUp() {
        super.onCleanUp();
        adapter = null;
    }

}
