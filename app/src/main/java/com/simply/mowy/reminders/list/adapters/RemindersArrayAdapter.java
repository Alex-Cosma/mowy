package com.simply.mowy.reminders.list.adapters;

import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;
import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.reminders.list.adapters.listeners.OnItemViewClickedListener;
import com.simply.mowy.reminders.list.adapters.listeners.OnSetDueDateButtonClickListener;
import com.simply.mowy.reminders.list.adapters.listeners.OnSetStartDateButtonClickListener;
import com.simply.mowy.reminders.list.adapters.listeners.ReminderPriorityChangedListener;
import com.simply.mowy.reminders.list.adapters.listeners.ReminderTextChangedListener;
import com.simply.mowy.reminders.list.adapters.listeners.SoftKeyboardCloser;
import com.simply.mowy.reminders.list.adapters.listeners.swipe.SwipeResultActionPin;
import com.simply.mowy.reminders.list.adapters.listeners.swipe.SwipeResultActionRemove;
import com.simply.mowy.reminders.list.adapters.listeners.swipe.SwipeResultActionUnpin;
import com.simply.mowy.utilities.DateTimeService;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.OnClickListener;
import static android.view.View.VISIBLE;
import static com.simply.mowy.utilities.Constants.COLOR_FOR_REMINDERS;
import static com.simply.mowy.utilities.Constants.MAX_SWIPE_LEFT_AMOUNT;
import static com.simply.mowy.utilities.Constants.MAX_SWIPE_RIGHT_AMOUNT;

/**
 * Created by Alex on 2/18/2017.
 */

public class RemindersArrayAdapter extends RecyclerView.Adapter<ReminderViewHolder> implements ReminderChangedListener, SwipeableItemAdapter<ReminderViewHolder> {

    protected final RemindersActivity activity;
    private final List<Reminder> reminders;
    private final ReminderService reminderService;
    private final DateTimeService dateTimeService;
    private final OnClickListener swipeableViewContainerOnClickListener;
    private final int neutralBackgroundResource;
    protected OnClickListener onSetStartDateButtonClickListener;
    protected OnClickListener onSetDueDateButtonClickListener;
    protected SwipeResultActionRemove swipeResultActionRemove;

    public RemindersArrayAdapter(RemindersActivity activity, List<Reminder> reminders, ReminderService reminderService, DateTimeService dateTimeService) {
        this.activity = activity;
        this.reminders = reminders;
        this.reminderService = reminderService;
        this.dateTimeService = dateTimeService;
        this.swipeResultActionRemove = new SwipeResultActionRemove(this, reminders, activity, reminderService);
        this.swipeableViewContainerOnClickListener = new OnItemViewClickedListener(this, activity);
        this.onSetStartDateButtonClickListener = new OnSetStartDateButtonClickListener(this, activity, reminders, reminderService, dateTimeService);
        this.onSetDueDateButtonClickListener = new OnSetDueDateButtonClickListener(this, reminders, reminderService, dateTimeService, activity);

        TypedArray ta = activity.obtainStyledAttributes(new int[]{android.R.attr.selectableItemBackground});
        this.neutralBackgroundResource = ta.getResourceId(0, 0);
        ta.recycle();

        setHasStableIds(true);
    }

    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReminderPriorityChangedListener reminderPriorityChangedListener = new ReminderPriorityChangedListener(reminderService, reminders, this);
        SoftKeyboardCloser softKeyboardCloser = new SoftKeyboardCloser(this.activity);
        ReminderTextChangedListener reminderTextChangedListener = new ReminderTextChangedListener(reminderService, reminders, softKeyboardCloser);
        return new ReminderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_entry_reminder, parent, false),
                reminderPriorityChangedListener, reminderTextChangedListener, softKeyboardCloser);
    }

    @Override
    public void onBindViewHolder(final ReminderViewHolder holder, int position) {
        final Reminder reminder = reminders.get(position);
        holder.reminderTextChangedListener.updatePosition(position);
        holder.reminderPriorityChangedListener.updatePosition(position);
        holder.text.setText(reminder.getText());
        holder.priority.setItems(activity.getResources().getStringArray(reminder.hasDueDate() ? R.array.priorities_with_auto : R.array.priorities));
        holder.priority.setSelectedIndex(reminder.getPriority());
        holder.priority.setArrowColor(COLOR_FOR_REMINDERS.get(reminder.getPriority()));
        holder.softKeyboardCloser.setFocusedView(holder.text);

        holder.containerView.setOnClickListener(swipeableViewContainerOnClickListener);
        holder.itemView.setOnClickListener(swipeableViewContainerOnClickListener);
        holder.btnSetStartDate.setOnClickListener(onSetStartDateButtonClickListener);
        holder.btnSetDueDate.setOnClickListener(onSetDueDateButtonClickListener);
        holder.rlBehindViews.setVisibility(reminder.isPinned() ? VISIBLE : GONE);

        holder.btnSetStartDate.setText(dateTimeService.formatDateForSetDateButton(reminder));
        holder.tvStartDateLabel.setText(dateTimeService.formatTextForStartDateLabel(reminder));
        holder.tvStartDateLabel.setVisibility(reminder.hasStartDate() ? VISIBLE : GONE);

        holder.btnSetDueDate.setText(dateTimeService.formatDateForSetDueDateButton(reminder));
        holder.tvDueDateLabel.setText(dateTimeService.formatTextForDueDateLabel(reminder));
        holder.tvDueDateTime.setText(dateTimeService.formatTextForDueDateTime(reminder));
        holder.tvDueDateTime.setVisibility(reminder.hasDueDate() ? VISIBLE : GONE);
        holder.tvDueDateLabel.setVisibility(reminder.hasDueDate() ? VISIBLE : GONE);

        // set swiping properties
        holder.setMaxRightSwipeAmount(MAX_SWIPE_RIGHT_AMOUNT);
        holder.setMaxLeftSwipeAmount(MAX_SWIPE_LEFT_AMOUNT);
        holder.setSwipeItemHorizontalSlideAmount(reminder.isPinned() ? MAX_SWIPE_RIGHT_AMOUNT : 0);
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    @Override
    public long getItemId(int position) {
        return reminders.get(position).getId();
    }

    @Override
    public void reminderChanged(int position) {
        notifyItemChanged(position);
    }

    @Override
    public int onGetSwipeReactionType(ReminderViewHolder holder, int position, int x, int y) {
        return SwipeableItemConstants.REACTION_CAN_SWIPE_BOTH_H;
    }

    @Override
    public SwipeResultAction onSwipeItem(ReminderViewHolder holder, int position, int result) {
        switch (result) {
            case SwipeableItemConstants.RESULT_SWIPED_RIGHT:
                return new SwipeResultActionPin(this, position);
            case SwipeableItemConstants.RESULT_SWIPED_LEFT:
                holder.itemView.setBackgroundResource(neutralBackgroundResource);
                if (reminders.get(position).isPinned()) {
                    return new SwipeResultActionUnpin(this, position);
                }
                swipeResultActionRemove.setPosition(position);
                return swipeResultActionRemove;
            case SwipeableItemConstants.RESULT_CANCELED:
            default:
                holder.itemView.setBackgroundResource(neutralBackgroundResource);
                if (position != RecyclerView.NO_POSITION) {
                    return new SwipeResultActionUnpin(this, position);
                } else {
                    return null;
                }
        }
    }

    @Override
    public void onSetSwipeBackground(ReminderViewHolder holder, int position, int type) {
        switch (type) {
            case SwipeableItemConstants.DRAWABLE_SWIPE_NEUTRAL_BACKGROUND:
                holder.rlBehindViews.setVisibility(GONE);
                holder.itemView.setBackgroundResource(neutralBackgroundResource);
                break;
            case SwipeableItemConstants.DRAWABLE_SWIPE_LEFT_BACKGROUND:
                holder.itemView.setBackgroundResource(R.drawable.bg_swipe_item_right);
                break;
            case SwipeableItemConstants.DRAWABLE_SWIPE_RIGHT_BACKGROUND:
                holder.rlBehindViews.setVisibility(VISIBLE);
                holder.itemView.setBackgroundResource(neutralBackgroundResource);
                break;
        }
    }

    public Reminder getReminder(int position) {
        return reminders.get(position);
    }
}

