package com.simply.mowy.reminders.list.adapters.listeners.swipe;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionMoveToSwipedDirection;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;

/**
 * Created by Alex on 3/13/2017.
 */

public class SwipeResultActionPin extends SwipeResultActionMoveToSwipedDirection {
    private final int position;
    private RemindersArrayAdapter adapter;

    public SwipeResultActionPin(RemindersArrayAdapter adapter, int position) {
        this.adapter = adapter;
        this.position = position;
    }

    @Override
    protected void onPerformAction() {
        super.onPerformAction();
        Reminder reminder = adapter.getReminder(position);
        if (!reminder.isPinned()) {
            reminder.setPinned(true);
            adapter.notifyItemChanged(position);
        }
    }

//    @Override
//    protected void onSlideAnimationEnd() {
//        super.onSlideAnimationEnd();
//
//        if (mSetPinned && adapter.mEventListener != null) {
//            mAdapter.mEventListener.onItemPinned(mPosition);
//        }
//    }

    @Override
    protected void onCleanUp() {
        super.onCleanUp();
        // clear the references
        adapter = null;
    }
}
