package com.simply.mowy.reminders.list.adapters.listeners;

import android.view.View;

import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.reminders.list.adapters.RemindersArrayAdapter;

/**
 * Created by Alex on 3/14/2017.
 */

public class OnItemViewClickedListener implements View.OnClickListener {

    private final RemindersArrayAdapter adapter;
    private final RemindersActivity activity;

    public OnItemViewClickedListener(RemindersArrayAdapter adapter, RemindersActivity activity) {
        this.adapter = adapter;
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        View actualView = RecyclerViewAdapterUtils.getParentViewHolderItemView(v);
        int position = activity.getRecyclerView().getChildAdapterPosition(actualView);
        Reminder data = adapter.getReminder(position);

        if (data.isPinned()) {
            data.setPinned(false);
            adapter.notifyItemChanged(position);
        }
    }
}
