package com.simply.mowy.reminders.list.adapters.listeners;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.adapters.ReminderChangedListener;

import java.util.List;

/**
 * Created by Alex on 2/18/2017.
 */

public class ReminderPriorityChangedListener implements MaterialSpinner.OnItemSelectedListener {

    private final ReminderService reminderService;
    private final List<Reminder> reminders;
    private final ReminderChangedListener reminderChangedCallback;
    private int position;

    public ReminderPriorityChangedListener(ReminderService reminderService, List<Reminder> reminders, ReminderChangedListener reminderChangedCallback) {
        this.reminderService = reminderService;
        this.reminders = reminders;
        this.reminderChangedCallback = reminderChangedCallback;
    }

    public void updatePosition(int position) {
        this.position = position;
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int priority, long id, Object item) {
        Reminder reminder = reminders.get(this.position);
        reminderService.setPriorityToReminder(reminder, priority);
        reminderChangedCallback.reminderChanged(this.position);
    }
}
