package com.simply.mowy.reminders.list.adapters.listeners;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.utilities.Constants;

/**
 * Created by Alex on 4/8/2017.
 */

public class SoftKeyboardCloser extends Handler {

    private final RemindersActivity activity;
    private EditText focusedView;
    private SoftKeyboardRunnable closeSoftKeyboardCallback;

    public SoftKeyboardCloser(RemindersActivity activity) {
        this.activity = activity;
    }

    void scheduleKeyboardClose() {
        closeSoftKeyboardCallback = new SoftKeyboardRunnable();
        postDelayed(closeSoftKeyboardCallback, Constants.CLOSE_SOFT_KEYBOARD_DELAY);
    }

    void cancelScheduledKeyboardClose() {
        removeCallbacks(closeSoftKeyboardCallback);
    }

    public void setFocusedView(EditText focusedView) {
        this.focusedView = focusedView;
    }

    private class SoftKeyboardRunnable implements Runnable {
        @Override
        public void run() {
            View currentFocusView = activity.getCurrentFocus();
            if (currentFocusView != null && currentFocusView.equals(focusedView)) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
                focusedView.clearFocus();
            }
        }
    }

}
