package com.simply.mowy.jobs;

import android.support.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 2/19/2017.
 */

public class ReminderBackgroundJob extends Job {

    static final String TAG = "REMINDER_BACKGROUND_JOB";
    private static final int RECURRENCE_PERIODICITY = 15; // minutes
    private static final int RECURRENCE_FLEX_TIME = 10; // minutes

    private final ReminderActivationService reminderActivationService;

    ReminderBackgroundJob(ReminderActivationService reminderActivationService) {
        this.reminderActivationService = reminderActivationService;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(TAG)
                .setRequiredNetworkType(JobRequest.NetworkType.ANY)
                .setRequiresDeviceIdle(false)
                .setRequiresCharging(false)
                .setUpdateCurrent(true)
                .setPersisted(true)
                .setPeriodic(TimeUnit.MINUTES.toMillis(RECURRENCE_PERIODICITY), TimeUnit.MINUTES.toMillis(RECURRENCE_FLEX_TIME))
                .build()
                .schedule();
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        reminderActivationService.runActivationForReminders();
        return Result.SUCCESS;
    }

}
