package com.simply.mowy.jobs;

import android.support.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 2/28/2017.
 */

public class ApplicationUsageBackgroundJob extends Job {
    public static final String TAG = "APPLICATION_USAGE_JOB";
    private static final int RECURRENCE_PERIODICITY = 48; // hours
    private static final int RECURRENCE_FLEX_TIME = 2; // hours

    private final ApplicationUsageService applicationUsageService;

    ApplicationUsageBackgroundJob(ApplicationUsageService applicationUsageService) {
        this.applicationUsageService = applicationUsageService;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(TAG)
                .setRequiredNetworkType(JobRequest.NetworkType.ANY)
                .setRequiresDeviceIdle(false)
                .setRequiresCharging(false)
                .setUpdateCurrent(true)
                .setPersisted(true)
                .setPeriodic(TimeUnit.HOURS.toMillis(RECURRENCE_PERIODICITY), TimeUnit.HOURS.toMillis(RECURRENCE_FLEX_TIME))
                .build()
                .schedule();
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        applicationUsageService.runApplicationUsageNotificationCheck();
        return Result.SUCCESS;
    }
}
