package com.simply.mowy.jobs;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by Alex on 2/19/2017.
 */

public class JobFactory implements JobCreator {

    private final ReminderActivationService reminderActivationService;
    private final ApplicationUsageService applicationUsageService;

    public JobFactory(ReminderActivationService reminderActivationService, ApplicationUsageService applicationUsageService) {
        this.reminderActivationService = reminderActivationService;
        this.applicationUsageService = applicationUsageService;
    }

    @Override
    public Job create(String tag) {
        switch (tag) {
            case ReminderBackgroundJob.TAG:
                return new ReminderBackgroundJob(reminderActivationService);
            case ApplicationUsageBackgroundJob.TAG:
                return new ApplicationUsageBackgroundJob(applicationUsageService);
            default:
                return null;
        }
    }
}