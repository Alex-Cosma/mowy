package com.simply.mowy.jobs;

import com.simply.mowy.notifications.NotificationService;
import com.simply.mowy.utilities.AnalyticsService;
import com.simply.mowy.utilities.DateTimeService;
import com.simply.mowy.utilities.SharedPreferencesService;

import org.joda.time.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.simply.mowy.utilities.Constants.Analytics.Actions.SEND_APPLICATION_USAGE_NOTIFICATION;
import static com.simply.mowy.utilities.Constants.Analytics.Categories.REMINDERS;
import static com.simply.mowy.utilities.Constants.MIN_HOURS_SINCE_LAST_LOGIN_TO_NOTIFY;

/**
 * Created by Alex on 2/28/2017.
 */

@Singleton
public class ApplicationUsageService {

    private final DateTimeService dateTimeService;
    private final NotificationService notificationService;
    private final SharedPreferencesService sharedPreferencesService;
    private final AnalyticsService analyticsService;

    @Inject
    ApplicationUsageService(DateTimeService dateTimeService, NotificationService notificationService, SharedPreferencesService sharedPreferencesService, AnalyticsService analyticsService) {
        this.dateTimeService = dateTimeService;
        this.notificationService = notificationService;
        this.sharedPreferencesService = sharedPreferencesService;
        this.analyticsService = analyticsService;
    }

    void runApplicationUsageNotificationCheck() {
        if (shouldSendNotificationAboutApplicationUsage()) {
            notificationService.notifyForApplicationUsage();
            analyticsService.sendEvent(REMINDERS, SEND_APPLICATION_USAGE_NOTIFICATION);
        }
    }

    boolean shouldSendNotificationAboutApplicationUsage() {
        Long hoursPassedSinceLastLogin = new Duration(sharedPreferencesService.getTimeSinceLastLogin().withTimeAtStartOfDay(), dateTimeService.now().withTimeAtStartOfDay()).getStandardHours();

        return hoursPassedSinceLastLogin >= MIN_HOURS_SINCE_LAST_LOGIN_TO_NOTIFY;
    }

}
