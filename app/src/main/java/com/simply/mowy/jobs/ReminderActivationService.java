package com.simply.mowy.jobs;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.notifications.NotificationService;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.utilities.DateTimeService;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.simply.mowy.utilities.Constants.MIN_ACTIVATION_PER_PRIORITY;

/**
 * Created by Alex on 2/19/2017.
 */

@Singleton
public class ReminderActivationService {

    private final DateTimeService dateTimeService;
    private final NotificationService notificationService;
    private final ReminderService reminderService;

    @Inject
    ReminderActivationService(DateTimeService dateTimeService, NotificationService notificationService, ReminderService reminderService) {
        this.dateTimeService = dateTimeService;
        this.notificationService = notificationService;
        this.reminderService = reminderService;
    }

    void runActivationForReminders() {
        List<Reminder> reminders = Reminder.listAll(Reminder.class);
        for (Reminder r : reminders) {
            if (shouldActivateReminder(r)) {
                activateReminder(r);
            }
        }
    }

    public boolean shouldActivateReminder(Reminder reminder) {
        Long minutesPassedSinceLastActivation = new Duration(reminder.getLastActivationTime(), dateTimeService.now()).getStandardMinutes();
        Long minimumMinutesThatShouldPass = MIN_ACTIVATION_PER_PRIORITY.get(reminder.getPriority());

        DateTime today = dateTimeService.now().withTimeAtStartOfDay();
        DateTime startDay = reminder.getStartDate().withTimeAtStartOfDay();

        boolean startDateBeforeToday = !reminder.hasStartDate() || (today.isAfter(startDay) || today.isEqual(startDay));

        return minutesPassedSinceLastActivation >= minimumMinutesThatShouldPass && startDateBeforeToday;
    }

    private void activateReminder(Reminder reminder) {
        reminderService.resetLastActivationTime(reminder);
        notificationService.notifyForReminder(reminder);
    }


}
