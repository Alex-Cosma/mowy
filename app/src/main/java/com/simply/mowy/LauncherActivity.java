package com.simply.mowy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.reminders.list.tourguide.RemindersTourGuideActivity;
import com.simply.mowy.utilities.SharedPreferencesService;

import org.joda.time.DateTime;

import javax.inject.Inject;

/**
 * Created by Alex on 2/23/2017.
 */

public class LauncherActivity extends Activity {

    @Inject
    public SharedPreferencesService sharedPreferencesService;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        ((SimplyMowyApplication) getApplicationContext()).getComponentFactory().inject(this);

        sharedPreferencesService.setLastLoginTime(new DateTime());

        if (sharedPreferencesService.isFirstTimeRun()) {
            Intent myIntent = new Intent(LauncherActivity.this, RemindersTourGuideActivity.class);
            LauncherActivity.this.startActivity(myIntent);
        } else {
            Intent myIntent = new Intent(LauncherActivity.this, RemindersActivity.class);
            LauncherActivity.this.startActivity(myIntent);
        }
        finish();
    }

}
