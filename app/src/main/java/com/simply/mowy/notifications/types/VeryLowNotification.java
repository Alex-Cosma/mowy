package com.simply.mowy.notifications.types;

import android.content.Context;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;

/**
 * Created by Alex on 2/21/2017.
 */

public class VeryLowNotification extends BaseNotification {

    public VeryLowNotification(Context context, Reminder reminder) {
        super(context, reminder);
    }

    @Override
    protected void initializeNotification() {
        super.initializeNotification();
        setSmallIcon(R.mipmap.ic_low_priority_white_24dp);
    }
}
