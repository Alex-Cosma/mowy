package com.simply.mowy.notifications.types;

import android.content.Context;
import android.graphics.Color;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;

/**
 * Created by Alex on 2/21/2017.
 */

public class LowNotification extends BaseNotification {

    public LowNotification(Context context, Reminder reminder) {
        super(context, reminder);
    }

    @Override
    protected void initializeNotification() {
        super.initializeNotification();
        setSmallIcon(R.mipmap.ic_event_note_white_24dp);
        setLights(Color.GREEN, 300, 1000);
        setVibrate(new long[]{300});
    }
}
