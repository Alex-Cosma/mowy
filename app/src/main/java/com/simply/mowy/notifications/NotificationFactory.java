package com.simply.mowy.notifications;

import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.model.ReminderBuilder;
import com.simply.mowy.notifications.types.ApplicationUsageNotification;
import com.simply.mowy.notifications.types.CriticalNotification;
import com.simply.mowy.notifications.types.HighNotification;
import com.simply.mowy.notifications.types.LowNotification;
import com.simply.mowy.notifications.types.MediumNotification;
import com.simply.mowy.notifications.types.TutorialNotification;
import com.simply.mowy.notifications.types.VeryLowNotification;

import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.simply.mowy.model.ReminderPriority.CRITICAL;
import static com.simply.mowy.model.ReminderPriority.getPriorityByValue;
import static com.simply.mowy.utilities.Constants.Actions.DECREASE_PRIORITY_ACTION;
import static com.simply.mowy.utilities.Constants.Actions.FINISH_REMINDER_ACTION;
import static com.simply.mowy.utilities.Constants.Actions.INCREASE_PRIORITY_ACTION;

/**
 * Created by Alex on 2/20/2017.
 */
@Singleton
public class NotificationFactory {

    private final NotificationActionFactory notificationActionFactory;
    private final Context context;

    @Inject
    NotificationFactory(NotificationActionFactory notificationActionFactory, Context context) {
        this.notificationActionFactory = notificationActionFactory;
        this.context = context;
    }

    NotificationCompat.Builder buildNotificationForReminder(Reminder reminder) {
        switch (getPriorityByValue(reminder.getPriority())) {
            case CRITICAL:
                return new CriticalNotification(context, reminder)
                        .addAction(notificationActionFactory.getAction(FINISH_REMINDER_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(DECREASE_PRIORITY_ACTION, reminder));
            case HIGH:
                return new HighNotification(context, reminder)
                        .addAction(notificationActionFactory.getAction(DECREASE_PRIORITY_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(FINISH_REMINDER_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(INCREASE_PRIORITY_ACTION, reminder));
            case MEDIUM:
                return new MediumNotification(context, reminder)
                        .addAction(notificationActionFactory.getAction(DECREASE_PRIORITY_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(FINISH_REMINDER_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(INCREASE_PRIORITY_ACTION, reminder));
            case LOW:
                return new LowNotification(context, reminder)
                        .addAction(notificationActionFactory.getAction(DECREASE_PRIORITY_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(FINISH_REMINDER_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(INCREASE_PRIORITY_ACTION, reminder));
            case VERY_LOW:
                return new VeryLowNotification(context, reminder)
                        .addAction(notificationActionFactory.getAction(FINISH_REMINDER_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(INCREASE_PRIORITY_ACTION, reminder));
            default:
                return new MediumNotification(context, reminder)
                        .addAction(notificationActionFactory.getAction(DECREASE_PRIORITY_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(FINISH_REMINDER_ACTION, reminder))
                        .addAction(notificationActionFactory.getAction(INCREASE_PRIORITY_ACTION, reminder));
        }
    }

    NotificationCompat.Builder buildNotificationForApplicationUsage() {
        String[] descriptionTexts = context.getResources().getStringArray(R.array.application_usage_description_text);
        Reminder reminder = new ReminderBuilder()
                .setId(-1L)
                .setText(descriptionTexts[new Random().nextInt(descriptionTexts.length - 1)])
                .setPriority(CRITICAL.getValue())
                .build();

        return new ApplicationUsageNotification(context, reminder);
    }

    NotificationCompat.Builder buildNotificationForTutorial(Reminder reminder) {
        return new TutorialNotification(context, reminder);
    }

}
