package com.simply.mowy.notifications.types;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;

/**
 * Created by Alex on 2/28/2017.
 */

public class ApplicationUsageNotification extends BaseNotification {

    public ApplicationUsageNotification(Context context, Reminder reminder) {
        super(context, reminder);
        setSmallIcon(R.mipmap.ic_warning_white_24dp);
        setLights(Color.RED, 100, 100);
        setVibrate(new long[]{500, 500, 500, 500, 500});
    }

    @NonNull
    @Override
    protected String getNotificationTitle() {
        return context.getResources().getString(R.string.application_usage_notification_title);
    }

}
