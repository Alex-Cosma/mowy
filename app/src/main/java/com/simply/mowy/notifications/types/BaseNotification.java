package com.simply.mowy.notifications.types;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.RemindersActivity;

import static com.simply.mowy.model.ReminderPriority.getPriorityByValue;

/**
 * Created by Alex on 2/21/2017.
 */

abstract class BaseNotification extends NotificationCompat.Builder {

    protected final Context context;
    private final Reminder reminder;

    BaseNotification(Context context, Reminder reminder) {
        super(context);
        this.context = context;
        this.reminder = reminder;
        initializeNotification();

        setContentIntent(getPendingIntent(context));
    }

    protected PendingIntent getPendingIntent(Context context) {
        Intent resultIntent = new Intent(context, RemindersActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);
        return stackBuilder.getPendingIntent(
                reminder.getId().intValue(),
                PendingIntent.FLAG_UPDATE_CURRENT
        );
    }

    protected void initializeNotification() {
        setContentText(reminder.getText());
        setAutoCancel(true);
        setContentTitle(getNotificationTitle());
    }

    @NonNull
    protected String getNotificationTitle() {
        return getPriorityByValue(reminder.getPriority()).getName() + " notification!";
    }
}
