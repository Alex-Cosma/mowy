package com.simply.mowy.notifications;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.simply.mowy.model.Reminder;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Alex on 2/20/2017.
 */
@Singleton
public class NotificationService {

    private final NotificationFactory notificationFactory;
    private final NotificationManager systemService;

    @Inject
    NotificationService(NotificationFactory notificationFactory, Context context) {
        this.notificationFactory = notificationFactory;
        this.systemService = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void notifyForReminder(Reminder reminder) {
        NotificationCompat.Builder builder = notificationFactory.buildNotificationForReminder(reminder);
        systemService.notify(reminder.getId().intValue(), builder.build());
    }

    public void notifyForApplicationUsage() {
        NotificationCompat.Builder builder = notificationFactory.buildNotificationForApplicationUsage();
        systemService.notify(-1, builder.build());
    }

    public void notifyForTutorialReminder(Reminder reminder) {
        NotificationCompat.Builder builder = notificationFactory.buildNotificationForTutorial(reminder);
        systemService.notify(-1, builder.build());
    }
}
