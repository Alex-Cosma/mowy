package com.simply.mowy.notifications.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.simply.mowy.SimplyMowyApplication;
import com.simply.mowy.reminders.list.ReminderService;

import static com.simply.mowy.utilities.Constants.Actions.DECREASE_PRIORITY_ACTION;
import static com.simply.mowy.utilities.Constants.Actions.FINISH_REMINDER_ACTION;
import static com.simply.mowy.utilities.Constants.Actions.INCREASE_PRIORITY_ACTION;
import static com.simply.mowy.utilities.Constants.BundleExtras.REMINDER_ID;

/**
 * Created by Alex on 2/21/2017.
 */

public class NotificationBroadcastReceiver extends BroadcastReceiver {

    ReminderService reminderService;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        reminderService = ((SimplyMowyApplication) context.getApplicationContext()).getComponentFactory().getReminderService();
        NotificationManager systemService = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Long reminderId = intent.getLongExtra(REMINDER_ID, -1);

        switch (action) {
            case FINISH_REMINDER_ACTION:
                reminderService.deleteReminderById(reminderId);
                break;
            case INCREASE_PRIORITY_ACTION:
                reminderService.increasePriority(reminderId);
                break;
            case DECREASE_PRIORITY_ACTION:
                reminderService.decreasePriority(reminderId);
                break;
        }

        systemService.cancel(reminderId.intValue());
        Log.v("ACTION", action);
    }
}
