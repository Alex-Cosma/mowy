package com.simply.mowy.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.utilities.Constants;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.simply.mowy.utilities.Constants.BundleExtras.REMINDER_ID;

/**
 * Created by Alex on 2/21/2017.
 */
@Singleton
public class NotificationActionFactory {

    private final Context context;

    @Inject
    NotificationActionFactory(Context context) {
        this.context = context;
    }

    NotificationCompat.Action getAction(String action, Reminder reminder) {
        Intent intent = new Intent();
        intent.putExtra(REMINDER_ID, reminder.getId());
        intent.setAction(action);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, reminder.getId().intValue(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        switch (action) {
            case Constants.Actions.FINISH_REMINDER_ACTION:
                return new NotificationCompat.Action(R.mipmap.ic_done_white_24dp, context.getString(R.string.done), pendingIntent);

            case Constants.Actions.INCREASE_PRIORITY_ACTION:
                return new NotificationCompat.Action(R.mipmap.ic_arrow_upward_white_24dp, context.getString(R.string.increase_priority), pendingIntent);

            case Constants.Actions.DECREASE_PRIORITY_ACTION:
                return new NotificationCompat.Action(R.mipmap.ic_arrow_downward_white_24dp, context.getString(R.string.decrease_priority), pendingIntent);

            default:
                return null; //TODO: exception
        }
    }

}
