package com.simply.mowy.notifications.types;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.TaskStackBuilder;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;
import com.simply.mowy.reminders.list.tourguide.RemindersTourGuideActivity;

import static com.simply.mowy.utilities.Constants.BundleExtras.FROM_TUTORIAL_NOTIFICATION;

/**
 * Created by Alex on 3/4/2017.
 */

public class TutorialNotification extends CriticalNotification {

    public TutorialNotification(Context context, Reminder reminder) {
        super(context, reminder);
    }

    protected PendingIntent getPendingIntent(Context context) {
        Intent resultIntent = new Intent(context, RemindersTourGuideActivity.class);
        resultIntent.putExtra(FROM_TUTORIAL_NOTIFICATION, true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);
        return stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
    }

    @NonNull
    @Override
    protected String getNotificationTitle() {
        return context.getResources().getString(R.string.tutorial_notification_top_title);
    }
}
