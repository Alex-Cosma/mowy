package com.simply.mowy.notifications.types;

import android.content.Context;
import android.graphics.Color;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;

/**
 * Created by Alex on 2/21/2017.
 */

public class MediumNotification extends BaseNotification {

    public MediumNotification(Context context, Reminder reminder) {
        super(context, reminder);
    }

    @Override
    protected void initializeNotification() {
        super.initializeNotification();
        setSmallIcon(R.mipmap.ic_access_time_white_24dp);
        setLights(Color.YELLOW, 200, 500);
        setVibrate(new long[]{1000});
    }
}
