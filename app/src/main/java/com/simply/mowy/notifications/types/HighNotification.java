package com.simply.mowy.notifications.types;

import android.content.Context;
import android.graphics.Color;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;

/**
 * Created by Alex on 2/21/2017.
 */

public class HighNotification extends BaseNotification {

    public HighNotification(Context context, Reminder reminder) {
        super(context, reminder);
    }

    @Override
    protected void initializeNotification() {
        super.initializeNotification();
        setSmallIcon(R.mipmap.ic_priority_high_white_24dp);
        setLights(Color.argb(1, 255, 127, 80), 300, 300);
        setVibrate(new long[]{500, 500, 500});
    }
}
