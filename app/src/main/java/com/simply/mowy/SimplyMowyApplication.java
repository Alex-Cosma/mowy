package com.simply.mowy;

import com.evernote.android.job.JobManager;
import com.orm.SchemaGenerator;
import com.orm.SugarApp;
import com.orm.SugarContext;
import com.orm.SugarDb;
import com.simply.mowy.jobs.ApplicationUsageBackgroundJob;
import com.simply.mowy.jobs.JobFactory;
import com.simply.mowy.jobs.ReminderBackgroundJob;

/**
 * Created by Alex on 2/18/2017.
 */
public class SimplyMowyApplication extends SugarApp {

    private SimplyMowyApplicationComponent componentFactory;

    public SimplyMowyApplicationComponent getComponentFactory() {
        return componentFactory;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDatabase();

        componentFactory = DaggerSimplyMowyApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .build();

        initializeJobScheduling();
    }

    private void initializeJobScheduling() {
        JobManager.create(this).addJobCreator(new JobFactory(componentFactory.getReminderActivationService(), componentFactory.getApplicationUsageService()));
        ReminderBackgroundJob.scheduleJob();
        JobManager.instance().cancelAllForTag(ApplicationUsageBackgroundJob.TAG); // TODO: remove, it's temporary
    }

    private void initializeDatabase() {
        SugarContext.init(getApplicationContext());
        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
        schemaGenerator.createDatabase(new SugarDb(this).getDB());
    }
}
