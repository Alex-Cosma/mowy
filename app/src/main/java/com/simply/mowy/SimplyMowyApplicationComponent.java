package com.simply.mowy;

import com.simply.mowy.jobs.ApplicationUsageService;
import com.simply.mowy.jobs.ReminderActivationService;
import com.simply.mowy.notifications.NotificationService;
import com.simply.mowy.reminders.list.ReminderService;
import com.simply.mowy.reminders.list.RemindersActivity;
import com.simply.mowy.utilities.AnalyticsService;
import com.simply.mowy.utilities.DateTimeService;
import com.simply.mowy.utilities.SharedPreferencesService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alex on 2/22/2017.
 */

@Component(modules = ContextModule.class)
@Singleton
public interface SimplyMowyApplicationComponent {

    NotificationService getNotificationService();

    ReminderActivationService getReminderActivationService();

    ApplicationUsageService getApplicationUsageService();

    ReminderService getReminderService();

    SharedPreferencesService getSharedPreferencesService();

    AnalyticsService getAnalyticsService();

    DateTimeService getDateTimeService();

    void inject(RemindersActivity activity);

    void inject(LauncherActivity activity);
}
