package com.simply.mowy.utilities;

import android.content.Context;
import android.content.ContextWrapper;

import com.pixplicity.easyprefs.library.Prefs;

import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.simply.mowy.utilities.Constants.SharedPreferences.FIRST_TIME_RUN;
import static com.simply.mowy.utilities.Constants.SharedPreferences.LAST_LOGIN_TIME;

/**
 * Created by Alex on 2/27/2017.
 */
@Singleton
public class SharedPreferencesService {

    @Inject
    public SharedPreferencesService(Context context) {
        new Prefs.Builder()
                .setContext(context)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(context.getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public Boolean isFirstTimeRun() {
        if (!Prefs.contains(FIRST_TIME_RUN)) {
            Prefs.putBoolean(FIRST_TIME_RUN, false);
            return true;
        }
        return false;
    }

    public void setLastLoginTime(DateTime date) {
        Prefs.putLong(LAST_LOGIN_TIME, date.getMillis());
    }

    public DateTime getTimeSinceLastLogin() {
        return new DateTime(Prefs.getLong(LAST_LOGIN_TIME, new DateTime().getMillis()));
    }

}
