package com.simply.mowy.utilities;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.simply.mowy.BuildConfig;
import com.simply.mowy.R;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Alex on 3/3/2017.
 */
@Singleton
public class AnalyticsService {

    private Tracker tracker;

    @Inject
    public AnalyticsService(Context context) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        tracker = analytics.newTracker(R.xml.global_tracker);
        if (BuildConfig.DEBUG) {
            GoogleAnalytics.getInstance(context).setDryRun(true);
        }
        tracker.enableAutoActivityTracking(true);
    }

    public void sendEvent(String category, String action) {
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .build());
    }

    public void sendEvent(String category, String action, String label) {
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }
}
