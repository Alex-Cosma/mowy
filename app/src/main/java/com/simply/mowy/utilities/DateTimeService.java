package com.simply.mowy.utilities;

import android.content.Context;
import android.support.annotation.NonNull;

import com.simply.mowy.R;
import com.simply.mowy.model.Reminder;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.simply.mowy.utilities.Constants.DATE_FORMAT;
import static com.simply.mowy.utilities.Constants.TIME_FORMAT;

/**
 * Created by Alex on 2/19/2017.
 */

@Singleton
public class DateTimeService {

    @Inject
    protected Context context;

    @Inject
    DateTimeService() {
    }

    public DateTime now() {
        return new DateTime();
    }

    public String formatDateForSetDateButton(Reminder reminder) {
        if (!reminder.hasStartDate()) {
            return context.getResources().getString(R.string.set_start_date);
        } else if (dateIsYesterday(reminder.getStartDate())) {
            return context.getString(R.string.yesterday);
        } else if (dateIsToday(reminder.getStartDate())) {
            return context.getString(R.string.today);
        } else if (dateIsTomorrow(reminder.getStartDate())) {
            return context.getString(R.string.tomorrow);
        } else {
            Locale currentLocale = context.getResources().getConfiguration().locale;
            return DATE_FORMAT.withLocale(currentLocale).print(reminder.getStartDate().getMillis());
        }
    }

    public String formatDateForSetDueDateButton(Reminder reminder) {
        if (!reminder.hasDueDate()) {
            return context.getResources().getString(R.string.set_due_date);
        } else if (dateIsYesterday(reminder.getDueDate())) {
            return context.getString(R.string.yesterday);
        } else if (dateIsToday(reminder.getDueDate())) {
            return context.getString(R.string.today);
        } else if (dateIsTomorrow(reminder.getDueDate())) {
            return context.getString(R.string.tomorrow);
        } else {
            Locale currentLocale = context.getResources().getConfiguration().locale;
            return DATE_FORMAT.withLocale(currentLocale).print(reminder.getDueDate().getMillis());
        }
    }


    public String formatTextForStartDateLabel(Reminder reminder) {
        if (reminder.getStartDate().withTimeAtStartOfDay().isAfter(now().withTimeAtStartOfDay())) {
            return context.getString(R.string.starts);
        } else {
            return context.getString(R.string.started);
        }
    }

    public String formatTextForDueDateLabel(Reminder reminder) {
        return context.getString(R.string.due);
    }

    public String formatTextForDueDateTime(Reminder reminder) {
        if(reminder.hasDueDate()) {
            Locale currentLocale = context.getResources().getConfiguration().locale;
            return TIME_FORMAT.withLocale(currentLocale).print(reminder.getDueDate().getMillis());
        } else {
            return "";
        }
    }

    @NonNull
    public DateTime formDate(int year, int monthOfYear, int dayOfMonth) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.YEAR, year);
        now.set(Calendar.MONTH, monthOfYear);
        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        return new DateTime(now);
    }

    private boolean dateIsYesterday(DateTime startDate) {
        return sameDates(startDate, now().minusDays(1));
    }

    private boolean dateIsToday(DateTime startDate) {
        return sameDates(startDate, now());
    }

    private boolean dateIsTomorrow(DateTime startDate) {
        return sameDates(startDate, now().plusDays(1));
    }

    private boolean sameDates(DateTime date1, DateTime date2) {
        return date1.getYear() == date2.getYear() && date1.getDayOfYear() == date2.getDayOfYear();
    }

}
