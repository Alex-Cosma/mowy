package com.simply.mowy.utilities;

import android.util.SparseIntArray;
import android.util.SparseLongArray;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.concurrent.TimeUnit;

import static android.graphics.Color.rgb;
import static com.simply.mowy.model.ReminderPriority.AUTO;
import static com.simply.mowy.model.ReminderPriority.CRITICAL;
import static com.simply.mowy.model.ReminderPriority.HIGH;
import static com.simply.mowy.model.ReminderPriority.LOW;
import static com.simply.mowy.model.ReminderPriority.MEDIUM;
import static com.simply.mowy.model.ReminderPriority.VERY_LOW;

/**
 * Created by Alex on 2/19/2017.
 */

public class Constants {
    public static final Long MIN_HOURS_SINCE_LAST_LOGIN_TO_NOTIFY = 48L;

    public static final SparseLongArray MIN_ACTIVATION_PER_PRIORITY = new SparseLongArray();
    public static final SparseIntArray COLOR_FOR_REMINDERS = new SparseIntArray();
    public static final int PENDING_REMOVAL_TIMEOUT_FOR_DELETING_REMINDER = 2000;
    public static final int PROGRESS_INDICATOR_VISIBLE_TIME = 667;
    public static final int DELAY_FOR_UNPINNING_AFTER_DATE_SELECTION = 667;
    public static final float MAX_SWIPE_RIGHT_AMOUNT = 0.75f;
    public static final float MAX_SWIPE_LEFT_AMOUNT = -1f;

    public static final String START_DATE_PICKER_DIALOG_TAG = "START_DATE_PICKER_DIALOG_TAG";
    public static final String DUE_DATE_DATE_PICKER_DIALOG_TAG = "DUE_DATE_DATE_PICKER_DIALOG_TAG";
    public static final String DUE_DATE_TIME_PICKER_DIALOG_TAG = "DUE_DATE_TIME_PICKER_DIALOG_TAG";
    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy");
    public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat.forPattern("dd/MM/yyyy hh:mm");
    public static final DateTimeFormatter TIME_FORMAT = DateTimeFormat.forPattern("k:mm");
    public static final int CLOSE_SOFT_KEYBOARD_DELAY = 2000;

    static {
        MIN_ACTIVATION_PER_PRIORITY.put(CRITICAL.getValue(), TimeUnit.MINUTES.toMinutes(15));
        MIN_ACTIVATION_PER_PRIORITY.put(HIGH.getValue(), TimeUnit.HOURS.toMinutes(2));
        MIN_ACTIVATION_PER_PRIORITY.put(MEDIUM.getValue(), TimeUnit.HOURS.toMinutes(6));
        MIN_ACTIVATION_PER_PRIORITY.put(LOW.getValue(), TimeUnit.HOURS.toMinutes(12));
        MIN_ACTIVATION_PER_PRIORITY.put(VERY_LOW.getValue(), TimeUnit.DAYS.toMinutes(1));

        COLOR_FOR_REMINDERS.put(AUTO.getValue(), rgb(240, 98, 146));
        COLOR_FOR_REMINDERS.put(CRITICAL.getValue(), rgb(183, 28, 28));
        COLOR_FOR_REMINDERS.put(HIGH.getValue(), rgb(255, 109, 0));
        COLOR_FOR_REMINDERS.put(MEDIUM.getValue(), rgb(255, 214, 0));
        COLOR_FOR_REMINDERS.put(LOW.getValue(), rgb(102, 187, 106));
        COLOR_FOR_REMINDERS.put(VERY_LOW.getValue(), rgb(41, 182, 246));
    }

    public static class Actions {
        public static final String FINISH_REMINDER_ACTION = "FINISH_REMINDER_ACTION";
        public static final String INCREASE_PRIORITY_ACTION = "INCREASE_PRIORITY_ACTION";
        public static final String DECREASE_PRIORITY_ACTION = "DECREASE_PRIORITY_ACTION";
        public static final String SHOW_LOADING_FOR_ACTIVITY = "SHOW_LOADING_FOR_ACTIVITY";
    }

    public static class BundleExtras {
        public static final String REMINDER_ID = "REMINDER_ID";
        public static final String FROM_TUTORIAL_NOTIFICATION = "FROM_TUTORIAL_NOTIFICATION";
    }

    static class SharedPreferences {
        static final String FIRST_TIME_RUN = "FIRST_TIME_RUN";
        static final String LAST_LOGIN_TIME = "LAST_LOGIN_TIME";
    }

    public static class Analytics {
        public static class Categories {
            public static final String REMINDERS = "Reminders";
        }

        public static class Actions {
            public static final String ADD_REMINDER = "Added reminder";
            public static final String REMOVE_REMINDER = "Remove reminder";
            public static final String INCREASE_PRIORITY = "Increase priority";
            public static final String DECREASE_PRIORITY = "Decrease priority";
            public static final String SET_PRIORITY = "Set priority";
            public static final String SEND_APPLICATION_USAGE_NOTIFICATION = "Send application usage notification";
        }
    }
}
