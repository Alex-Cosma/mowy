package com.simply.mowy.model;

/**
 * Created by Alex on 2/19/2017.
 */

public enum ReminderPriority {

    VERY_LOW(0, "Very low"),
    LOW(1, "Low"),
    MEDIUM(2, "Medium"),
    HIGH(3, "High"),
    CRITICAL(4, "Critical"),
    AUTO(5, "Auto");

    private final Integer value;
    private final String name;

    ReminderPriority(final Integer newValue, final String name) {
        this.value = newValue;
        this.name = name;
    }

    public static ReminderPriority getPriorityByValue(Integer value) {
        switch (value) {
            case 5:
                return AUTO;
            case 4:
                return CRITICAL;
            case 3:
                return HIGH;
            case 2:
                return MEDIUM;
            case 1:
                return LOW;
            case 0:
                return VERY_LOW;
            default:
                return MEDIUM;
        }
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
