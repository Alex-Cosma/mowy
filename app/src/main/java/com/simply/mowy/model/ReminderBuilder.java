package com.simply.mowy.model;

import org.joda.time.DateTime;

/**
 * Created by Alex on 3/10/2017.
 */

public class ReminderBuilder {

    private Reminder reminder;

    public ReminderBuilder() {
        reminder = new Reminder();
    }

    public ReminderBuilder setText(String text) {
        reminder.setText(text);
        return this;
    }

    public ReminderBuilder setPriority(Integer priority) {
        reminder.setPriority(priority);
        return this;
    }

    public ReminderBuilder setLastActivationTime(DateTime lastActivationTime) {
        reminder.setLastActivationTime(lastActivationTime);
        return this;
    }

    public ReminderBuilder setId(Long id) {
        reminder.setId(id);
        return this;
    }

    public ReminderBuilder setStartDate(DateTime startDate) {
        reminder.setStartDate(startDate);
        return this;
    }

    public Reminder build() {
        return reminder;
    }

}
