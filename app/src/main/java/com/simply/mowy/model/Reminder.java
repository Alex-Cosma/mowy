package com.simply.mowy.model;


import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import org.joda.time.DateTime;

/**
 * Created by Alex on 2/18/2017.
 */

public class Reminder extends SugarRecord {

    private String text;
    private Integer priority;
    private Long lastActivationTime;
    private Long startDate;
    private Long dueDate;

    @Ignore
    private boolean isPinned;

    public Reminder() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public DateTime getLastActivationTime() {
        return new DateTime(lastActivationTime);
    }

    public void setLastActivationTime(DateTime lastActivationTime) {
        this.lastActivationTime = lastActivationTime.getMillis();
    }

    public boolean isPinned() {
        return isPinned;
    }

    public void setPinned(boolean pinned) {
        isPinned = pinned;
    }

    public DateTime getStartDate() {
        return new DateTime(startDate);
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate.getMillis();
    }

    public boolean hasStartDate() {
        return startDate != null;
    }

    public DateTime getDueDate() {
        return new DateTime(dueDate);
    }

    public void setDueDate(DateTime dueDate) {
        this.dueDate = dueDate.getMillis();
    }

    public boolean hasDueDate() {
        return dueDate != null;
    }
}
