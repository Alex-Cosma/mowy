package com.simply.mowy;

import com.simply.mowy.model.Reminder;
import com.simply.mowy.model.ReminderBuilder;
import com.simply.mowy.utilities.DateTimeService;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import static com.simply.mowy.model.ReminderPriority.CRITICAL;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(RobolectricTestRunner.class)
public class SimplyRemindMeUnitTests {

    @Test
    public void dateTimeServiceMockingTest() {
        DateTimeService dateTimeService = Mockito.mock(DateTimeService.class);

        DateTime mockedDateTime = new DateTime().withDate(1, 1, 1).withTime(1, 1, 1, 1);
        when(dateTimeService.now()).thenReturn(mockedDateTime);
        Reminder reminder = new ReminderBuilder()
                .setText("Reminder Text")
                .setPriority(CRITICAL.getValue())
                .setLastActivationTime(dateTimeService.now())
                .build();

        Assert.assertEquals(mockedDateTime, reminder.getLastActivationTime());
    }

}